package utils;

import lombok.SneakyThrows;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static utils.LocatorExtensions.toBy;

@SuppressWarnings("unused")
public class SearchContextExtensions {

    public static WebElement getElement(WebDriver driver, WebElementLocator locator) {
        return driver.findElement(toBy(locator));
    }

    public static List<WebElement> getElements(WebDriver driver, WebElementLocator locator) {
        return driver.findElements(toBy(locator));
    }

    public static String getElementText(WebDriver driver, WebElementLocator locator) {
        List<WebElement> webElement = driver.findElements(toBy(locator));
        if (webElement.size() == 0) {
            return "Web element not present inside HTML DOM. With locator: " + locator.getValue();
        } else {
            return webElement.get(0).getText();
        }
    }

    public static void waitForClickableWebElement(WebDriver driver, WebElementLocator element, long timeout) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.elementToBeClickable(toBy(element)));
    }

    public static void waitForVisibleWebElement(WebDriver driver, WebElementLocator element, long timeout) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.visibilityOfElementLocated(toBy(element)));
    }

    public static void waitForInvisibilityOfWebElement(WebDriver driver, WebElementLocator element, long timeout) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(toBy(element)));
    }

    public static void waitForWebElementCountChange(WebDriver driver, WebElementLocator element, int elementsToBe, long timeout) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.numberOfElementsToBe(toBy(element), elementsToBe));
    }


    public static void scrollToWithJS(WebDriver driver, WebElement webElement) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", webElement);
    }

    @SneakyThrows
    public static void scrollPageToBottom(WebDriver driver, WebElementLocator locator, WebElementLocator componentSpinnerLoader) {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        WebElement webElement = driver.findElement(toBy(locator));
        Long scrollHeight;
        Long scrollActualPositoon;
        int scrollBy = 1000;
        int scrollBaseHeight = Math.toIntExact((Long) js.executeScript("return arguments[0].scrollHeight", webElement));

        if (scrollBaseHeight > 2000) {
            do {
                js.executeScript("arguments[0].scrollTo({ top: " + scrollBy + ", behavior: 'smooth' })", webElement);
                do {
                    Thread.sleep(100);
                    scrollActualPositoon = (Long) js.executeScript("return arguments[0].scrollTop", webElement);
                } while (scrollActualPositoon < scrollBy);
                waitForAllComponentsSpinnersToBeInvisible(driver, componentSpinnerLoader);
                scrollHeight = (Long) js.executeScript("return arguments[0].scrollHeight", webElement);
                scrollBy += 1000;
            } while (scrollBy < scrollHeight - 1000);
        }
    }

    public static void waitForAllComponentsSpinnersToBeInvisible(WebDriver driver, WebElementLocator componentSpinnerLoader) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(Constants.LONG_TIMEOUT));
        int numberOfLoadersOnPage = driver.findElements(toBy(componentSpinnerLoader)).size();
        if (numberOfLoadersOnPage != 0) {
           for (int i = numberOfLoadersOnPage; i > 0; i--) {
               wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(String.format("(" + componentSpinnerLoader.getValue() + ")[%s]", i))));
           }
        }
    }

    public static boolean waitForSpecificUrl(WebDriver driver, String expectedUrl, long timeout) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        return wait.until(ExpectedConditions.urlMatches(expectedUrl));
    }
}
