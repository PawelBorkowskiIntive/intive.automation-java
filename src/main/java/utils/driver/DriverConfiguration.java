package utils.driver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.*;
import utils.Constants;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DriverConfiguration {
    @Getter
    private WebDriver webDriver;

    @SneakyThrows
    @Before("@UI")
    public void setUp(Scenario scenario) {
        WebDriver driver;
        String browser = System.getProperty("browser");
        String gridUrl = System.getProperty("gridUrl");
        System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver");
        boolean isHeadless = Optional.ofNullable(System.getProperty("isHeadless")).map(Boolean::valueOf).orElse(false);
        System.setProperty("webdriver.chrome.silentOutput", "true");
        Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);

        if ("Chrome".equals(browser)) {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setCapability("applicationCacheEnabled", false);
            chromeOptions.addArguments("--disable-plugins", "--disable-extensions", "--disable-popup-blocking");
            chromeOptions.addArguments("enable-automation");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--disable-dev-shm-usage");
            chromeOptions.addArguments("--window-size=1920x1080");
            chromeOptions.addArguments("--disable-browser-side-navigation");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--ignore-certificate-errors");
            chromeOptions.addArguments("start-maximized");
            chromeOptions.addArguments("force-device-scale-factor=1");
            chromeOptions.addArguments("high-dpi-support=1");
            chromeOptions.setHeadless(isHeadless);
            Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(chromeOptions);
            driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(Constants.PAGELOADTIMEOUT));
            driver.manage().window().maximize();
            webDriver = driver;
        } else if ("FireFox".equals(browser)) {
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setCapability("applicationCacheEnabled", false);
            firefoxOptions.addArguments("--disable-plugins", "--disable-extensions", "--disable-popup-blocking");
            firefoxOptions.addArguments("enable-automation");
            firefoxOptions.addArguments("--no-sandbox");
            firefoxOptions.addArguments("--disable-dev-shm-usage");
            firefoxOptions.addArguments("--disable-browser-side-navigation");
            firefoxOptions.addArguments("--disable-gpu");
            firefoxOptions.addArguments("--ignore-certificate-errors");
            firefoxOptions.addArguments("force-device-scale-factor=1");
            firefoxOptions.addArguments("high-dpi-support=1");
            firefoxOptions.setHeadless(isHeadless);
            WebDriverManager.firefoxdriver().driverVersion("0.30.0").setup();
            driver = new FirefoxDriver(firefoxOptions);
            driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(Constants.PAGELOADTIMEOUT));
            driver.manage().window().maximize();
            webDriver = driver;
        } else if ("Remote".equals(browser) && !gridUrl.equals("gridUrl")) {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setCapability("applicationCacheEnabled", false);
            chromeOptions.addArguments("--disable-plugins", "--disable-extensions", "--disable-popup-blocking");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--ignore-certificate-errors");
            chromeOptions.setHeadless(isHeadless);
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(CapabilityType.BROWSER_NAME, Browser.CHROME);
            capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
            capabilities.setCapability("screenResolution", "1920x1080");
            capabilities.setCapability("name", scenario.getName());
//            capabilities.setCapability("recordVideo", false);
            webDriver = new RemoteWebDriver(new URL(gridUrl), capabilities);
        }
    }

    @After("@UI")
    public void afterTest(Scenario scenario) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        if (scenario.isFailed()) {
            String filename = String.format("%s %s.jpg", scenario.getName(), simpleDateFormat.format(date));
            Allure.addAttachment(filename, new ByteArrayInputStream(((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES)));
        }

        if (webDriver != null) {
            webDriver.quit();
        }
    }
}
