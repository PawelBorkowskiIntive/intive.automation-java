package utils.driver;

import org.openqa.selenium.WebDriver;

public abstract class DriverPage {

    protected final WebDriver baseDriver;
    protected final DriverConfiguration baseDriverConfiguration;

    public DriverPage(DriverConfiguration driverConfiguration) {
        baseDriver = driverConfiguration.getWebDriver();
        baseDriverConfiguration = driverConfiguration;
    }
}
