package utils;

import org.openqa.selenium.WebDriver;

@SuppressWarnings("unused")
public class UrlHandler {

    public static String getUrlAlias(String urlFull) {
        return urlFull.replace(System.getProperty("url"), "");
    }

    public static String getUrlLastAlias(WebDriver driver) {
        String url = driver.getCurrentUrl();
        String[] urlSplit = url.split("/");
        return urlSplit[urlSplit.length-1];
    }
}
