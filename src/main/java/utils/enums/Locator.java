package utils.enums;

public enum Locator {
    Id,
    ClassName,
    CssSelector,
    LinkText,
    Name,
    PartialLinkText,
    TagName,
    Xpath,
}
