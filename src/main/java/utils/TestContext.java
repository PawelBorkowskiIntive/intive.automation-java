package utils;

import io.cucumber.java.After;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@SuppressWarnings("unused")
public class TestContext {
    private WebDriver secondWebDriver;
    private String cookie;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Map<Object, Object> commonContextMapOfObjects = new HashMap<>();

    public void setCommonContextMapOfObjects(Object key, Object value) {
        commonContextMapOfObjects.put(key, value);
    }

    public Object getCommonContextMapOfObjects(Object key) {
        return commonContextMapOfObjects.get(key);
    }

    @After
    public void killDriver() {
        if (secondWebDriver != null) {
            secondWebDriver.quit();
        }
    }
}
