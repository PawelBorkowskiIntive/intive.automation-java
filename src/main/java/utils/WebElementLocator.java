package utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import utils.enums.Locator;

@AllArgsConstructor
@Getter
@Setter
public class WebElementLocator {

    private Locator kind;
    private String value;

    public WebElementLocator format(Object... args) {
        return new WebElementLocator(this.kind, String.format(this.value, args));
    }
}
