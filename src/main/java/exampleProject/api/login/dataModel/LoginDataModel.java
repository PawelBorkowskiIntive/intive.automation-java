package exampleProject.api.login.dataModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDataModel {
    private String email;
    private String password;
    private String accessToken;
}
