package exampleProject.api.login;

import exampleProject.api.login.dataModel.LoginDataModel;
import utils.endpointHandlers.BaseEndpoint;
import utils.endpointHandlers.IEndpoint;

public class Login_Endpoint extends BaseEndpoint implements IEndpoint {

    private final Class<LoginDataModel[]> classType = LoginDataModel[].class;
    public final String path = "/login";

    @Override
    public void convertResponseToDataModel(String response) {
        super.convertJsonToDataModelArray(response, classType);
    }

    public Login_Endpoint() {
        super.initRequestBody(classType, "exampleProject/api/login/LoginRequestBody.json");
    }

    public String getAccessToken() {
        return super.getDataModelAsArray(classType)[0].getAccessToken();
    }
}
