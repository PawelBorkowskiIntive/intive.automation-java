package exampleProject.api.booking.dataModel;

import lombok.Data;

@Data
public class BookingDataModel {
    private Integer bookingid;
    private Booking booking;

    @Data
    public static class Booking {
        private String firstname;
        private String lastname;
        private Integer totalprice;
        private Boolean depositpaid;
        private Bookingdates bookingdates;
        private String additionalneeds;
    }

    @Data
    public static class Bookingdates {
        private String checkin;
        private String checkout;
    }
}
