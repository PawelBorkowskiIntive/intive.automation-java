package exampleProject.api.booking;

import exampleProject.api.booking.dataModel.BookingDataModel;
import utils.endpointHandlers.BaseEndpoint;
import utils.endpointHandlers.IEndpoint;

public class Booking_Endpoint extends BaseEndpoint implements IEndpoint {

    private final Class<BookingDataModel[]> classType = BookingDataModel[].class;
    private final Class<BookingDataModel.Booking[]> classTypeSimple = BookingDataModel.Booking[].class;
    private boolean isSimpleResponse = false;

    public final String path = "/booking/{id}";

    public Booking_Endpoint() {
        super.initRequestBody(classTypeSimple, "exampleProject/api/booking/BookingRequestBody.json");
    }

    public void setIsSimpleResponse(boolean isSimpleResponse) {
        this.isSimpleResponse = isSimpleResponse;
    }

    @Override
    public void convertResponseToDataModel(String response) {
        if (isSimpleResponse) {
            super.convertJsonToDataModelArray(response, classTypeSimple);
        } else {
            super.convertJsonToDataModelArray(response, classType);
        }
    }

    public void setBookingData(BookingDataModel.Booking bookingData) {
        BookingDataModel.Booking booking = super.getDataModelAsArray(classTypeSimple)[0];
        booking.setFirstname(bookingData.getFirstname());
        booking.setLastname(bookingData.getLastname());
        booking.setTotalprice(bookingData.getTotalprice());
    }

    public BookingDataModel.Booking getBookingData() {
        return super.getDataModelAsArray(classType)[0].getBooking();
    }

    public BookingDataModel.Booking getSimpleBookingData() {
        return super.getDataModelAsArray(classTypeSimple)[0];
    }

    public Integer getBookingId() {
        return super.getDataModelAsArray(classType)[0].getBookingid();
    }
}
