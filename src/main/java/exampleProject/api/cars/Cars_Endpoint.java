package exampleProject.api.cars;

import exampleProject.api.cars.dataModel.CarsDataModel;
import utils.endpointHandlers.BaseEndpoint;
import utils.endpointHandlers.IEndpoint;

import java.util.Arrays;

public class Cars_Endpoint extends BaseEndpoint implements IEndpoint {

    private final Class<CarsDataModel[]> classType = CarsDataModel[].class;
    public final String path = "/660/cars";

    public Cars_Endpoint() {
        super.initRequestBody(classType, "exampleProject/api/cars/CarsRequestBody.json");
    }

    @Override
    public void convertResponseToDataModel(String response) {
        super.convertJsonToDataModelArray(response, classType);
    }

    public CarsDataModel getCarDataByModel(String model) {
        return Arrays.stream(super.getDataModelAsArray(classType))
                .filter(el -> el.getModel().equals(model)).findFirst().orElse(null);
    }

}
