package exampleProject.api.cars.dataModelNew;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarsDataModelNew {
    private Integer id;
}
