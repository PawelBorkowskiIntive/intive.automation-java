package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class SecurityPackage {
    private Boolean bulletproofWindows;
    private Boolean steelSecuredBody;
    private Boolean antiExplosionFloor;
    private SpecialTires specialTires;
    private HealthMonitorSystem healthMonitorSystem;

    @Data
    private static class SpecialTires {
        private Integer tireTread;
        private String tireReinforcement;
        private String tirePressureSensor;
    }

    @Data
    private static class HealthMonitorSystem {
        private BackupAirSystem backupAirSystem;
        private FirstAid firstAid;
    }

    @Data
    private static class BackupAirSystem {
        private Integer airCapacity;
        private String airPowerAuxiliary;
    }

    @Data
    private static class FirstAid {
        private Boolean severedLimbsAid;
        private GunShotWoundsAid gunShotWoundsAid;
    }

    @Data
    private static class GunShotWoundsAid {
        private Boolean cal5_6mmAid;
        private Boolean cal7_62mmAid;
        private Boolean cal9mmAid;
        private Boolean cal9_5mmAid;
        private Boolean cal13mmAid;
    }
}
