package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class ColorDetails {
    private String color;
    private String code;
}
