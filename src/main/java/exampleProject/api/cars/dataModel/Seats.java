package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class Seats {
    private Boolean heated;
    private Boolean ventilated;
    private Boolean massage;
}
