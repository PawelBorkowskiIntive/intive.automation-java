package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class Engine {
    private Integer displacement;
    private String cylinderSystem;
    private Integer power;
}
