package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class ProductionHistory {
    private String date;
    private String description;
}
