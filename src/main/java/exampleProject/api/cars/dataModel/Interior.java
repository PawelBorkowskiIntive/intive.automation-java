package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class Interior {
    private Seats frontSeats;
    private Seats backSeats;
    private SecurityPackage securityPackage;
    private Extras extras;
}
