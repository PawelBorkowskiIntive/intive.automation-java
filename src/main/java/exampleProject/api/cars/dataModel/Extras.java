package exampleProject.api.cars.dataModel;

import lombok.Data;

@Data
public class Extras {
    private Boolean heatedSteeringWheel;
    private Boolean smokerPackage;
    private Boolean selfParkingSystem;
    private Boolean electricCurtains;
    private Boolean bmwIndividual;
    private Boolean removedEmblems;
}
