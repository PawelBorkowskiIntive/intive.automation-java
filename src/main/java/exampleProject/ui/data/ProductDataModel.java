package exampleProject.ui.data;

import lombok.Data;

@Data
public class ProductDataModel {
    private String name;
    private String description;
    private String price;
    private String buttonStatus;
}
