package exampleProject.ui.data;

import lombok.Data;

@Data
public class SummaryInformationDataModel {
    private String paymentInformation;
    private String shippingInformation;
    private String itemTotal;
    private String tax;
    private String total;
}
