package exampleProject.ui.data;

import lombok.Data;

@Data
public class ClientDataModel {
    private String firstName;
    private String lastName;
    private String zipCode;
}
