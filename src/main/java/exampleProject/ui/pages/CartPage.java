package exampleProject.ui.pages;

import exampleProject.ui.data.ProductDataModel;
import org.openqa.selenium.WebDriver;
import utils.WebElementLocator;
import utils.driver.DriverConfiguration;
import utils.enums.Locator;

import java.util.ArrayList;
import java.util.List;

import static utils.SearchContextExtensions.*;

@SuppressWarnings("UnusedReturnValue")
public class CartPage extends InventoryPage {

    private final WebDriver driver = super.baseDriver;
    private final DriverConfiguration driverConfiguration = super.baseDriverConfiguration;

    private final WebElementLocator
            cart_CheckoutButton = new WebElementLocator(Locator.Id, "checkout"),
            product_Component = new WebElementLocator(Locator.CssSelector, ".cart_item"),
            product_DataByNameAndOrderNumber = new WebElementLocator(Locator.Xpath, "(//div[@class='inventory_item_%s'])[%s]"),
            product_GenericButton = new WebElementLocator(Locator.Xpath, "(//div[@class='item_pricebar']//button)[%s]");

    public CartPage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public CheckoutStepOnePage clickCheckoutButton() {
        getElement(driver, cart_CheckoutButton).click();
        return new CheckoutStepOnePage(driverConfiguration);
    }

    public List<ProductDataModel> getCartProductsData() {
        int productsCount = getElements(driver, product_Component).size();
        List<ProductDataModel> productsData = new ArrayList<>();
        for (int i = 1; i <= productsCount ; i++) {
            ProductDataModel productData = new ProductDataModel();
            productData.setName(getElementText(driver, product_DataByNameAndOrderNumber.format("name", i)));
            productData.setDescription(getElementText(driver, product_DataByNameAndOrderNumber.format("desc", i)));
            productData.setPrice(getElementText(driver, product_DataByNameAndOrderNumber.format("price", i)));
            productData.setButtonStatus(getElementText(driver, product_GenericButton.format(i)));
            productsData.add(productData);
        }
        return productsData;
    }
}
