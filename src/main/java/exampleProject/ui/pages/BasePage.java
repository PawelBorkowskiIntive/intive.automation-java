package exampleProject.ui.pages;

import org.openqa.selenium.WebDriver;
import utils.Constants;
import utils.driver.DriverConfiguration;
import utils.driver.DriverPage;

import static utils.SearchContextExtensions.waitForSpecificUrl;

public class BasePage extends DriverPage {

    private final WebDriver driver = super.baseDriver;

    public BasePage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public void goToUrl() {
        String landingPageAlias = "/";
        String host = System.getProperty("url");
        driver.get(host + landingPageAlias);
    }

    public boolean waitForSpecificPage(String expectedPage) {
        return waitForSpecificUrl(driver, expectedPage + "$", Constants.MEDIUM2_TIMEOUT);
    }
}
