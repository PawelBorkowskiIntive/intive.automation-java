package exampleProject.ui.pages;

import org.openqa.selenium.WebDriver;
import utils.WebElementLocator;
import utils.driver.DriverConfiguration;
import utils.enums.Locator;

import static utils.SearchContextExtensions.getElement;

@SuppressWarnings("UnusedReturnValue")
public class CheckoutStepOnePage extends CartPage {

    private final WebDriver driver = super.baseDriver;
    private final DriverConfiguration driverConfiguration = super.baseDriverConfiguration;

    private final WebElementLocator
            continueButton = new WebElementLocator(Locator.Id, "continue"),
            firstNameInput = new WebElementLocator(Locator.Id, "first-name"),
            lastNameInput = new WebElementLocator(Locator.Id, "last-name"),
            zipCodeInput = new WebElementLocator(Locator.Id, "postal-code");

    public CheckoutStepOnePage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public CheckoutStepOnePage setFirstName(String firstName) {
        getElement(driver, firstNameInput).sendKeys(firstName);
        return this;
    }

    public CheckoutStepOnePage setLastName(String lastName) {
        getElement(driver, lastNameInput).sendKeys(lastName);
        return this;
    }

    public CheckoutStepOnePage setZipCode(String zipCode) {
        getElement(driver, zipCodeInput).sendKeys(zipCode);
        return this;
    }

    public CheckoutStepTwoPage clickContinueButton() {
        getElement(driver, continueButton).click();
        return new CheckoutStepTwoPage(driverConfiguration);
    }
}
