package exampleProject.ui.pages;

import org.openqa.selenium.WebDriver;
import utils.WebElementLocator;
import utils.driver.DriverConfiguration;
import utils.enums.Locator;

import static utils.SearchContextExtensions.getElement;

@SuppressWarnings("UnusedReturnValue")
public class LoginPage extends BasePage {

    private final WebDriver driver = super.baseDriver;
    private final DriverConfiguration driverConfiguration = super.baseDriverConfiguration;

    private final WebElementLocator
            login_userNameInput = new WebElementLocator(Locator.Id, "user-name"),
            login_passwordInput = new WebElementLocator(Locator.Id, "password"),
            login_loginButton = new WebElementLocator(Locator.Id, "login-button");

    public LoginPage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public LoginPage setUserName(String username) {
        getElement(driver, login_userNameInput).sendKeys(username);
        return this;
    }

    public LoginPage setUserPassword(String userPassword) {
        getElement(driver, login_passwordInput).sendKeys(userPassword);
        return this;
    }

    public InventoryPage clickLoginButton() {
        getElement(driver, login_loginButton).click();
        return new InventoryPage(driverConfiguration);
    }


}
