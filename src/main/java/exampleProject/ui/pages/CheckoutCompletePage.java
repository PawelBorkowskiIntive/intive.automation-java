package exampleProject.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.WebElementLocator;
import utils.driver.DriverConfiguration;
import utils.enums.Locator;

import java.util.List;
import java.util.stream.Collectors;

import static utils.SearchContextExtensions.getElements;

public class CheckoutCompletePage extends CheckoutStepTwoPage {

    private final WebDriver driver = super.baseDriver;

    private final WebElementLocator
            confirmationMessages = new WebElementLocator(Locator.Xpath, "//div[@id='checkout_complete_container']/*[contains(@class,'complete')]");

    public CheckoutCompletePage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public List<String> getConfirmationMessages() {
        return getElements(driver, confirmationMessages).stream().map(WebElement::getText).collect(Collectors.toList());
    }
}
