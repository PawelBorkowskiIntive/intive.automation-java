package exampleProject.ui.pages;

import exampleProject.ui.data.SummaryInformationDataModel;
import exampleProject.ui.data.ProductDataModel;
import org.openqa.selenium.WebDriver;
import utils.WebElementLocator;
import utils.driver.DriverConfiguration;
import utils.enums.Locator;

import java.util.ArrayList;
import java.util.List;

import static utils.SearchContextExtensions.*;

@SuppressWarnings("UnusedReturnValue")
public class CheckoutStepTwoPage extends CheckoutStepOnePage {

    private final WebDriver driver = super.baseDriver;
    private final DriverConfiguration driverConfiguration = super.baseDriverConfiguration;

    private final WebElementLocator
            finishButton = new WebElementLocator(Locator.Id, "finish"),
            product_Component = new WebElementLocator(Locator.CssSelector, ".cart_item"),
            product_DataByNameAndOrderNumber = new WebElementLocator(Locator.Xpath, "(//div[@class='inventory_item_%s'])[%s]"),
            product_GenericButton = new WebElementLocator(Locator.Xpath, "(//div[@class='item_pricebar']//button)[%s]"),
            summary_DetailsByName = new WebElementLocator(Locator.Xpath, "//div[@class='summary_info']/div[@class='summary_%s']%s");

    public CheckoutStepTwoPage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public List<ProductDataModel> getCheckoutProductsData() {
        int productsCount = getElements(driver, product_Component).size();
        List<ProductDataModel> productsData = new ArrayList<>();
        for (int i = 1; i <= productsCount ; i++) {
            ProductDataModel productData = new ProductDataModel();
            productData.setName(getElementText(driver, product_DataByNameAndOrderNumber.format("name", i)));
            productData.setDescription(getElementText(driver, product_DataByNameAndOrderNumber.format("desc", i)));
            productData.setPrice(getElementText(driver, product_DataByNameAndOrderNumber.format("price", i)));
            productData.setButtonStatus(getElementText(driver, product_GenericButton.format(i)));
            productsData.add(productData);
        }
        return productsData;
    }

    public SummaryInformationDataModel getSummaryInformationData() {
        SummaryInformationDataModel summaryInformationData = new SummaryInformationDataModel();
        summaryInformationData.setPaymentInformation(getElementText(driver, summary_DetailsByName.format("value_label" , "[1]")));
        summaryInformationData.setShippingInformation(getElementText(driver, summary_DetailsByName.format("value_label" , "[2]")));
        summaryInformationData.setItemTotal(getElementText(driver, summary_DetailsByName.format("subtotal_label" , "")).split(": ")[1]);
        summaryInformationData.setTax(getElementText(driver, summary_DetailsByName.format("tax_label" , "")).split(": ")[1]);
        summaryInformationData.setTotal(getElementText(driver, summary_DetailsByName.format("total_label" , "")).split(": ")[1]);
        return summaryInformationData;
    }

    public CheckoutCompletePage clickFinishButton() {
        getElement(driver, finishButton).click();
        return new CheckoutCompletePage(driverConfiguration);
    }
}
