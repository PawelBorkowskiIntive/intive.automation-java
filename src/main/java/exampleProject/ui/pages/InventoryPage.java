package exampleProject.ui.pages;

import exampleProject.ui.data.ProductDataModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import utils.WebElementLocator;
import utils.driver.DriverConfiguration;
import utils.enums.Locator;

import java.util.ArrayList;
import java.util.List;

import static utils.SearchContextExtensions.*;

@SuppressWarnings("UnusedReturnValue")
public class InventoryPage extends LoginPage {

    private final WebDriver driver = super.baseDriver;
    private final DriverConfiguration driverConfiguration = super.baseDriverConfiguration;

    private final WebElementLocator
            header_ShoppingCartIcon = new WebElementLocator(Locator.CssSelector, ".shopping_cart_link"),
            header_SortingDropdown = new WebElementLocator(Locator.CssSelector, ".product_sort_container"),
            header_ShoppingCartProductCount = new WebElementLocator(Locator.CssSelector, ".shopping_cart_badge"),
            product_Component = new WebElementLocator(Locator.CssSelector, ".inventory_item"),
            product_DataByNameAndOrderNumber = new WebElementLocator(Locator.Xpath, "(//div[@class='inventory_item_%s'])[%s]"),
            product_GenericButton = new WebElementLocator(Locator.Xpath, "(//div[@class='pricebar']//button)[%s]"),
            product_ButtonByName = new WebElementLocator(Locator.Xpath, "//div[@class='inventory_item_name' and text()='%s']/parent::a/parent::div/following-sibling::div//button");

    public InventoryPage(DriverConfiguration driverConfiguration) {
        super(driverConfiguration);
    }

    public List<ProductDataModel> getProductsData() {
        int productsCount = getElements(driver, product_Component).size();
        List<ProductDataModel> productsData = new ArrayList<>();
        for (int i = 1; i <= productsCount ; i++) {
            ProductDataModel productData = new ProductDataModel();
            productData.setName(getElementText(driver, product_DataByNameAndOrderNumber.format("name", i)));
            productData.setDescription(getElementText(driver, product_DataByNameAndOrderNumber.format("desc", i)));
            productData.setPrice(getElementText(driver, product_DataByNameAndOrderNumber.format("price", i)));
            productData.setButtonStatus(getElementText(driver, product_GenericButton.format(i)));
            productsData.add(productData);
        }
        return productsData;
    }

    public InventoryPage clickAddToCartByName(String productName) {
        getElement(driver, product_ButtonByName.format(productName)).click();
        return this;
    }

    public String getButtonStatusByProductName(String productName) {
        return getElementText(driver, product_ButtonByName.format(productName));
    }

    public int getCartProductCount() {
        return Integer.parseInt(getElementText(driver, header_ShoppingCartProductCount));
    }

    public CartPage clickShoppingCartIcon() {
        getElement(driver, header_ShoppingCartIcon).click();
        return new CartPage(driverConfiguration);
    }

    public InventoryPage selectSorting(String sortingOption) {
        Select select = new Select(getElement(driver, header_SortingDropdown));
        select.selectByVisibleText(sortingOption);
        return this;
    }
}
