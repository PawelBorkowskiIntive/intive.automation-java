package testUtils;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.*;

@CucumberOptions(
        glue = {"utils",
                "common", "shared"},
        plugin = {"pretty", "summary", "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm"})
public class CommonRunnerConfig extends AbstractTestNGCucumberTests {

    @BeforeTest
    @Parameters({"url", "browser", "gridUrl"})
    public void init(String url, @Optional("browser") String browser, @Optional("gridUrl") String gridUrl) {
        System.setProperty("url", url);
        System.setProperty("browser", browser);
        System.setProperty("gridUrl", gridUrl);
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
