package testUtils;

import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static testUtils.ParseFactory.customTryParse;

public class DataTableTypeHandler {

    @SneakyThrows
    public static <E> E createObjectOfClass(Class<E> clazz, Map<String, String> entry) {
        List<String> listOfFields = Arrays.stream(clazz.getDeclaredFields()).map(Field::getName).collect(Collectors.toList());
        E object = clazz.getDeclaredConstructor().newInstance();
        for (String fieldName : listOfFields) {
            setValueOfField(fieldName, entry.get(fieldName), object);
        }
        return object;
    }

    private static void setValueOfField(String fieldName, String value, Object object) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(object, field.getType().getTypeName().contains("List") ? Collections.singletonList(customTryParse(value)) : customTryParse(value));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
