package exampleProject.ui.runner;

import io.cucumber.testng.CucumberOptions;
import testUtils.CommonRunnerConfig;

@CucumberOptions(
        features = {"src/test/resources/exampleProject/ui/features"},
        glue = {"exampleProject/ui/steps"})
public class ExamplePrjUITestRunner extends CommonRunnerConfig {
}
