package exampleProject.ui.steps;

import exampleProject.ui.data.ProductDataModel;
import exampleProject.ui.pages.CartPage;
import io.cucumber.java.After;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;

import java.util.List;

public class CartSteps {

    private final CartPage cartPage;
    private final SoftAssertions softAssertions = new SoftAssertions();

    public CartSteps(CartPage cartPage) {
        this.cartPage = cartPage;
    }

    //region When


    @When("User clicks checkout button on shopping cart page")
    public void clicksCheckoutButton() {
        cartPage
                .clickCheckoutButton();
    }

    //endregion

    //region Then
    @Then("Cart should have selected products")
    public void assertCartProducts(List<ProductDataModel> expectedProductsData) {
        //Arrange
        String stepName = "Cart should have selected products";

        //Act
        List<ProductDataModel> actualProductsData = cartPage.getCartProductsData();

        //Assert
        softAssertions.assertThat(actualProductsData).as(stepName).containsExactlyInAnyOrderElementsOf(expectedProductsData);

    }

    @Then("Shopping cart page should be displayed")
    public void assertInventoryPage() {
        //Act
        boolean actualPageAlias = cartPage.waitForSpecificPage("cart.html");

        //Assert
        Assertions.assertThat(actualPageAlias).as("Expected Shopping cart page was not loaded.").isTrue();
    }
    //endregion

    @After
    public void softAssert() {
        softAssertions.assertAll();
    }
}
