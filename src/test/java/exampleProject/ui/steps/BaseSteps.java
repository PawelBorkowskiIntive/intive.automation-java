package exampleProject.ui.steps;

import io.cucumber.java.en.When;
import exampleProject.ui.pages.BasePage;

public class BaseSteps {

    private final BasePage basePage;

    public BaseSteps(BasePage basePage) {
        this.basePage = basePage;
    }

    //region When
    @When("User goes to Sauce Demo page")
    public void userGoesToAdt() {
        basePage.goToUrl();
    }
    //endregion
}
