package exampleProject.ui.steps;

import exampleProject.ui.data.SummaryInformationDataModel;
import exampleProject.ui.data.ProductDataModel;
import exampleProject.ui.pages.CheckoutStepTwoPage;
import io.cucumber.java.After;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;

import java.util.List;
import java.util.Map;

public class CheckoutStepTwoSteps {

    private final CheckoutStepTwoPage checkoutStepTwoPage;
    private final SoftAssertions softAssertions = new SoftAssertions();

    public CheckoutStepTwoSteps(CheckoutStepTwoPage checkoutStepTwoPage) {
        this.checkoutStepTwoPage = checkoutStepTwoPage;
    }

    @When("Users clicks finish button to finalize transaction on checkout step two page")
    public void finalizeTransaction() {
        checkoutStepTwoPage.clickFinishButton();
    }

    //region Then
    @Then("Overview information should have product information checkout step two page")
    public void assertProductInfo(List<ProductDataModel> expectedProductsData) {
        //Arrange
        String stepName = "Overview information should have product information checkout step two page";

        //Act
        List<ProductDataModel> actualProductsData = checkoutStepTwoPage.getCheckoutProductsData();

        //Assert
        softAssertions
                .assertThat(actualProductsData)
                .usingRecursiveFieldByFieldElementComparatorIgnoringFields("buttonStatus")
                .as(stepName)
                .containsExactlyInAnyOrderElementsOf(expectedProductsData);
    }

    @DataTableType
    @SuppressWarnings("unused")
    public SummaryInformationDataModel createSummaryInformationObject(Map<String, String> entry) {
        SummaryInformationDataModel summaryInformationDataModel = new SummaryInformationDataModel();
        summaryInformationDataModel.setPaymentInformation(entry.get("paymentInformation"));
        summaryInformationDataModel.setShippingInformation(entry.get("shippingInformation"));
        summaryInformationDataModel.setItemTotal(entry.get("itemTotal"));
        summaryInformationDataModel.setTax(entry.get("tax"));
        summaryInformationDataModel.setTotal(entry.get("total"));
        return summaryInformationDataModel;
    }

    @Then("Overview information should have summary information on checkout step two page")
    public void assertOtherInformation(SummaryInformationDataModel expectedSummaryInformationData) {
        //Arrange
        String stepName = "Overview information should have summary information on checkout step two page";

        //Act
        SummaryInformationDataModel actualSummaryInformationData = checkoutStepTwoPage.getSummaryInformationData();

        //Assert
        softAssertions.assertThat(actualSummaryInformationData).as(stepName).isEqualTo(expectedSummaryInformationData);
    }

    @Then("Checkout step two page should be displayed")
    public void assertInventoryPage() {
        //Act
        boolean actualPageAlias = checkoutStepTwoPage.waitForSpecificPage("checkout-step-two.html");

        //Assert
        Assertions.assertThat(actualPageAlias).as("Expected Checkout step two page was not loaded.").isTrue();
    }
    //endregion

    @After
    public void softAssert() {
        softAssertions.assertAll();
    }
}
