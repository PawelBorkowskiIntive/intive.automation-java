package exampleProject.ui.steps;

import exampleProject.ui.pages.CheckoutCompletePage;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;

import java.util.List;

public class CheckoutCompleteSteps {

    private final CheckoutCompletePage checkoutCompletePage;

    public CheckoutCompleteSteps(CheckoutCompletePage checkoutCompletePage) {
        this.checkoutCompletePage = checkoutCompletePage;
    }

    //region Then
    @Then("Checkout complete page should be displayed")
    public void assertInventoryPage() {
        //Act
        boolean actualPageAlias = checkoutCompletePage.waitForSpecificPage("checkout-complete.html");

        //Assert
        Assertions.assertThat(actualPageAlias).as("Expected Checkout complete page was not loaded.").isTrue();
    }

    @Then("Order confirmation messages should be displayed")
    public void assertOrderConfirmationMessages(List<String> expectedMessages) {
        //Arrange
        String stepName = "Order confirmation messages should be displayed";

        //Act
        List<String> actualMessages = checkoutCompletePage.getConfirmationMessages();

        //Assert
        Assertions.assertThat(actualMessages).as(stepName).isEqualTo(expectedMessages);
    }
    //endregion
}
