package exampleProject.ui.steps;

import exampleProject.ui.data.ProductDataModel;
import exampleProject.ui.pages.InventoryPage;
import io.cucumber.java.After;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InventorySteps {

    private final InventoryPage inventoryPage;
    private final SoftAssertions softAssertions = new SoftAssertions();

    public InventorySteps(InventoryPage inventoryPage) {
        this.inventoryPage = inventoryPage;
    }

    //region When
    @When("User change sorting to {} on inventory page")
    public void changeSorting(String sortingOption) {
        inventoryPage
                .selectSorting(sortingOption);
    }

    @When("User adds to cart {} product on inventory page")
    public void addToCartProductByName(String productName) {
        inventoryPage
                .clickAddToCartByName(productName);
    }

    @When("User goes to shopping cart")
    public void goToShoppingCart() {
        inventoryPage.clickShoppingCartIcon();
    }
    //endregion

    //region Then


    @Then("^Product (.*) should have (ADD TO CART|REMOVE) visible button name$")
    public void assertProductButton(String productName, String expectedButtonStatus) {
        //Arrange
        String stepName = "^Product %s should have %s visible button name$";

        //Act
        String actualButtonStatus = inventoryPage.getButtonStatusByProductName(productName);

        //Assert
        softAssertions.assertThat(actualButtonStatus).as(stepName, productName, expectedButtonStatus).isEqualTo(expectedButtonStatus);
    }

    @DataTableType
    @SuppressWarnings("unused")
    public ProductDataModel createProductObject(Map<String, String> entry) {
        ProductDataModel productDataModel = new ProductDataModel();
        productDataModel.setName(entry.get("name"));
        productDataModel.setDescription(entry.get("description"));
        productDataModel.setPrice(entry.get("price"));
        productDataModel.setButtonStatus(entry.get("buttonStatus"));
        return productDataModel;
    }

    @Then("Product item should have details on inventory page")
    public void assertProductDetails(List<ProductDataModel> expectedProductData) {
        //Arrange
        String stepName = "Product item should have details on inventory page";

        //Act
        List<ProductDataModel> actualProductData = inventoryPage.getProductsData();

        //Assert
        softAssertions.assertThat(actualProductData).as(stepName).isEqualTo(expectedProductData);
    }

    @Then("Products names should be in order on inventory page")
    public void assertProductOrder(List<String> expectedProductOrder) {
        //Arrange
        String stepName = "Products names should be in order on inventory page";

        //Act
        List<String> actualProductData = inventoryPage.getProductsData().stream().map(ProductDataModel::getName).collect(Collectors.toList());

        //Assert
        softAssertions.assertThat(actualProductData).as(stepName).isEqualTo(expectedProductOrder);
    }

    @Then("Inventory page should be displayed")
    public void assertInventoryPage() {
        //Act
        boolean actualPageAlias = inventoryPage.waitForSpecificPage("inventory.html");

        //Assert
        Assertions.assertThat(actualPageAlias).as("Expected Inventory page was not loaded.").isTrue();
    }

    @Then("Cart should have {int} displayed on page header")
    public void assertCurtProductCount(int expectedProductCount) {
        //Arrange
        String stepName = "Cart should have %s displayed on page header";

        //Act
        int actualProductCount = inventoryPage.getCartProductCount();

        //Assert
        softAssertions.assertThat(actualProductCount).as(stepName, expectedProductCount).isEqualTo(expectedProductCount);
    }
    //endregion

    @After
    public void softAssert() {
        softAssertions.assertAll();
    }
}
