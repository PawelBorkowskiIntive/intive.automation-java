package exampleProject.ui.steps;

import exampleProject.ui.data.ClientDataModel;
import exampleProject.ui.pages.CheckoutStepOnePage;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

import java.util.Map;

@SuppressWarnings("unused")
public class CheckoutStepOneSteps {

    private final CheckoutStepOnePage checkoutStepOnePage;

    public CheckoutStepOneSteps(CheckoutStepOnePage checkoutStepOnePage) {
        this.checkoutStepOnePage = checkoutStepOnePage;
    }

    //region When
    @DataTableType
    public ClientDataModel createClientData(Map<String, String> entry) {
        ClientDataModel clientData = new ClientDataModel();
        clientData.setFirstName(entry.get("firstName"));
        clientData.setLastName(entry.get("lastName"));
        clientData.setZipCode(entry.get("zipCode"));
        return clientData;
    }

    @When("User fills out client information on checkout step one page")
    public void finalizeTransaction(ClientDataModel clientData) {
        checkoutStepOnePage
                .setFirstName(clientData.getFirstName())
                .setLastName(clientData.getLastName())
                .setZipCode(clientData.getZipCode())
                .clickContinueButton();
    }
    //endregion


    //region Then
    @Then("Checkout step one page should be displayed")
    public void assertInventoryPage() {
        //Act
        boolean actualPageAlias = checkoutStepOnePage.waitForSpecificPage("checkout-step-one.html");

        //Assert
        Assertions.assertThat(actualPageAlias).as("Expected Checkout step one page was not loaded.").isTrue();
    }
    //endregion
}
