package exampleProject.api.runner;

import io.cucumber.testng.CucumberOptions;
import testUtils.CommonRunnerConfig;

@CucumberOptions(
        features = {"src/test/resources/exampleProject/api/features"},
        glue = {"exampleProject/api/steps"})
public class ExamplePrjApiTestRunner extends CommonRunnerConfig {
}
