package exampleProject.api.steps.cars;

import exampleProject.api.cars.Cars_Endpoint;
import exampleProject.api.cars.dataModel.CarsDataModel;
import exampleProject.api.cars.dataModel.Engine;
import exampleProject.api.steps.common.Base_Steps;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import testUtils.RestAssuredContext;
import utils.TestContext;

import java.util.Map;

import static testUtils.ParseFactory.customTryParse;

public class Cars_Steps extends Base_Steps<Cars_Endpoint> {

    public Cars_Steps(TestContext testContext, RestAssuredContext restAssuredContext, Cars_Endpoint endpoint) {
        super(testContext, restAssuredContext);
        super.setEndpoint(endpoint);
    }

    //region When
    @When("User sends a valid POST request to cars")
    public void sendPostToCars() {
        sendPostRequest(endpoint.path, endpoint.getRequestBody());
    }

    @When("User sends a valid GET request to cars to get all")
    public void sendGetToCarsToGetAll() {
        sendGetRequest(endpoint.path);
    }
    //endregion

    //region Then
    @DataTableType
    public CarsDataModel createCarsObject(Map<String, String> entry) {
        CarsDataModel carsData = new CarsDataModel();
        carsData.setBrand(entry.get("brand"));
        carsData.setModel(entry.get("model"));
        carsData.setGeneration(customTryParse(entry.get("generation")));
        carsData.setEngine(new Engine() {{
            setDisplacement(customTryParse(entry.get("displacement")));
            setCylinderSystem(entry.get("cylinderSystem"));
            setPower(customTryParse(entry.get("power")));
        }});
        return carsData;
    }

    @Then("Cars response should have car details")
    public void assertCarsResponse(CarsDataModel expectedCarsData) {
        //Act
        CarsDataModel actualCarData = endpoint.getCarDataByModel(expectedCarsData.getModel());

        //Assert
        //Assertion when some fields are ignored so all other will be asserted no matter what will fail.
        RecursiveComparisonConfiguration comparisonConfiguration = new RecursiveComparisonConfiguration();
        comparisonConfiguration.setIgnoreAllExpectedNullFields(true);
        Assertions.assertThat(actualCarData).usingRecursiveComparison(comparisonConfiguration).isEqualTo(expectedCarsData);

        //Not so good assertion. If first will fail other will not be checked.
//        Assertions.assertThat(actualCarData.getEngine().getDisplacement()).isEqualTo(expectedCarsData.getEngine().getDisplacement());
//        Assertions.assertThat(actualCarData.getEngine().getPower()).isEqualTo(expectedCarsData.getEngine().getPower());
//        Assertions.assertThat(actualCarData.getEngine().getCylinderSystem()).isEqualTo(expectedCarsData.getEngine().getCylinderSystem());
    }
    //endregion
}
