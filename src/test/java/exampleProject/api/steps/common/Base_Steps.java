package exampleProject.api.steps.common;

import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.RequestSpecification;
import testUtils.RestAssuredContext;
import utils.endpointHandlers.IEndpoint;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import utils.TestContext;

import java.util.ArrayList;

@SuppressWarnings({"SameParameterValue", "unused"})
public class Base_Steps<T> {

    protected final TestContext testContext;
    protected final RestAssuredContext restAssuredContext;
    protected T endpoint;
    private IEndpoint iEndpoint;

    public void setEndpoint(T endpoint) {
        this.endpoint = endpoint;
        iEndpoint = (IEndpoint) endpoint;
    }

    public Base_Steps(TestContext testContext, RestAssuredContext restAssuredContext) {
        this.testContext = testContext;
        this.restAssuredContext = restAssuredContext;
    }

    private void deserialize(Response response) {
        String contentType = response.headers().getValue("content-type");
        if (contentType != null && contentType.contains("application/json")) {
            iEndpoint.convertResponseToDataModel(response.asString());
        }
    }

    //region GET
    protected void sendGetRequest(String path) {
        Response response = restAssuredContext.getRequestSpecification()
                .when()
                .body("")
                .get(path)
                .then().extract().response();
        restAssuredContext.setResponse(response);
        deserialize(response);
    }
    //endregion

    //region POST
    protected void sendPostRequest(String path, String requestBody) {
        Response response = restAssuredContext.getRequestSpecification()
                .when()
                .body(requestBody).log().body()
                .post(path)
                .then().extract().response();
        restAssuredContext.setResponse(response);
        deserialize(response);
    }
    //endregion

    //region PUT
    protected void sendPutRequest(String path, String requestBody) {
        Response response = restAssuredContext.getRequestSpecification()
                .when()
                .body(requestBody).log().body()
                .put(path)
                .then().extract().response();
        restAssuredContext.setResponse(response);
        deserialize(response);
    }
    //endregion

    //region DELETE
    protected void sendDeleteRequest(String path) {
        Response response = restAssuredContext.getRequestSpecification()
                .when()
                .body("")
                .delete(path)
                .then().extract().response();
        restAssuredContext.setResponse(response);
        deserialize(response);
    }
    //endregion

    //region GET with wait
    @SneakyThrows
    protected boolean sendGetRequestAndWaitForContent(String path, int maxWait)  {
        for (int i = 1; i <= maxWait; i++) {
            Thread.sleep(500);
            Response response = restAssuredContext.getRequestSpecification()
                    .when()
                    .get(path)
                    .then().extract().response();
            if (!response.asString().equals("[]")) {
                restAssuredContext.setResponse(response);
                deserialize(response);
                return true;
            }
        }
        return false;
    }
    //endregion

    protected void attachHeader(String key, String value) {
        RequestSpecification requestSpecification = restAssuredContext.getRequestSpecification();
        restAssuredContext.setRequestSpecification(requestSpecification.header(key, value));
    }

    protected void detachHeader(String key) {
        FilterableRequestSpecification requestSpecification = (FilterableRequestSpecification) restAssuredContext.getRequestSpecification();
        requestSpecification.removeHeader(key);
    }

    protected void removeCookie(String cookie) {
        FilterableRequestSpecification requestSpecification = (FilterableRequestSpecification) restAssuredContext.getRequestSpecification();
        requestSpecification.removeCookie(cookie);
    }

    protected void clearReqQueryParams() {
        FilterableRequestSpecification requestSpecification = (FilterableRequestSpecification) restAssuredContext.getRequestSpecification();
        new ArrayList<>(requestSpecification.getQueryParams().keySet()).forEach(requestSpecification::removeQueryParam);
    }
}
