package exampleProject.api.steps.booking;

import exampleProject.api.booking.Booking_Endpoint;
import exampleProject.api.booking.dataModel.BookingDataModel;
import exampleProject.api.steps.common.Base_Steps;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import testUtils.RestAssuredContext;
import utils.TestContext;
import utils.enums.ExampleProject;

import java.util.Map;

import static testUtils.EndpointsPathHandler.getPathWithNameParameter;
import static testUtils.ParseFactory.customTryParse;

public class Booking_Steps extends Base_Steps<Booking_Endpoint> {

    public Booking_Steps(TestContext testContext, RestAssuredContext restAssuredContext, Booking_Endpoint endpoint) {
        super(testContext, restAssuredContext);
        super.setEndpoint(endpoint);
    }

    //region When
    @DataTableType
    public BookingDataModel.Booking createBookingObject(Map<String, String> entry) {
        BookingDataModel.Booking bookingData = new BookingDataModel.Booking();
        bookingData.setFirstname(entry.get("firstname"));
        bookingData.setLastname(entry.get("lastname"));
        bookingData.setTotalprice(customTryParse(entry.get("totalprice")));
        if (entry.get("depositpaid") != null) {
            bookingData.setDepositpaid(customTryParse(entry.get("depositpaid")));
        }
        if (entry.get("additionalneeds") != null) {
            bookingData.setAdditionalneeds(customTryParse(entry.get("additionalneeds")));
        }
        if (entry.get("checkin") != null && entry.get("checkout") != null) {
            bookingData.setBookingdates(new BookingDataModel.Bookingdates() {{
                setCheckin(entry.get("checkin"));
                setCheckout(entry.get("checkout"));
            }});
        }
        return bookingData;
    }

    @When("Booking request is updated with")
    public void updateBookingRequest(BookingDataModel.Booking bookingData) {
        endpoint.setBookingData(bookingData);
    }

    @When("User sends a valid POST request to Booking")
    public void createNewBooking() {
        sendPostRequest(getPathWithNameParameter(endpoint.path, ""), endpoint.getRequestBody());
        testContext.setCommonContextMapOfObjects(ExampleProject.BOOKING_ID, endpoint.getBookingId());
    }

    @When("User sends a valid GET request to Booking looking for created booking by Id")
    public void getBookingById() {
        String bookingId = ((Integer) testContext.getCommonContextMapOfObjects(ExampleProject.BOOKING_ID)).toString();
        endpoint.setIsSimpleResponse(true);
        sendGetRequest(getPathWithNameParameter(endpoint.path, bookingId));
    }
    //endregion

    //region Then
    @Then("Created booking should have data")
    @Then("Booking should have data")
    public void assertCreatedBooking(BookingDataModel.Booking expectedBookingData) {
        //Act
        BookingDataModel.Booking actualBookingData = endpoint.getBookingData();

        //Assert
        Assertions.assertThat(actualBookingData.getFirstname()).isEqualTo(expectedBookingData.getFirstname());
        Assertions.assertThat(actualBookingData.getLastname()).isEqualTo(expectedBookingData.getLastname());
        Assertions.assertThat(actualBookingData.getTotalprice()).isEqualTo(expectedBookingData.getTotalprice());
    }

    @Then("Booking response should have data")
    public void assertSearchBooking(BookingDataModel.Booking expectedBookingData) {
        //Act
        BookingDataModel.Booking actualBookingData = endpoint.getSimpleBookingData();

        //Assert
        Assertions.assertThat(actualBookingData).isEqualTo(expectedBookingData);
    }
    //endregion
}
