package exampleProject.api.steps.login;

import exampleProject.api.login.Login_Endpoint;
import exampleProject.api.steps.common.Base_Steps;
import io.cucumber.java.en.When;
import testUtils.RestAssuredContext;
import utils.TestContext;

public class Login_Steps extends Base_Steps<Login_Endpoint> {

    public Login_Steps(TestContext testContext, RestAssuredContext restAssuredContext, Login_Endpoint endpoint) {
        super(testContext, restAssuredContext);
        super.setEndpoint(endpoint);
    }

    //region When
    @When("User sends a valid POST request to login")
    public void sendPostToLogin() {
        sendPostRequest(endpoint.path, endpoint.getRequestBody());
        restAssuredContext.getRequestSpecification().auth().preemptive().oauth2(endpoint.getAccessToken());
    }
    //endregion
}
