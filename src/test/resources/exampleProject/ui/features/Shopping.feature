@UI
Feature: Shopping activities
  @dev
  Scenario: SHP01 Logged in user should be able to to buy product and finalize transaction
    When User goes to Sauce Demo page
      And User logs in as Standard user on Login page
    Then Inventory page should be displayed
      And Product item should have details on inventory page
        | name                              | description                                                                                                                                                            | price  | buttonStatus |
        | Sauce Labs Backpack               | carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.                                 | $29.99 | ADD TO CART  |
        | Sauce Labs Bike Light             | A red light isn't the desired state in testing but it sure helps when riding your bike at night. Water-resistant with 3 lighting modes, 1 AAA battery included.        | $9.99  | ADD TO CART  |
        | Sauce Labs Bolt T-Shirt           | Get your testing superhero on with the Sauce Labs bolt T-shirt. From American Apparel, 100% ringspun combed cotton, heather gray with red bolt.                        | $15.99 | ADD TO CART  |
        | Sauce Labs Fleece Jacket          | It's not every day that you come across a midweight quarter-zip fleece jacket capable of handling everything from a relaxing day outdoors to a busy day at the office. | $49.99 | ADD TO CART  |
        | Sauce Labs Onesie                 | Rib snap infant onesie for the junior automation engineer in development. Reinforced 3-snap bottom closure, two-needle hemmed sleeved and bottom won't unravel.        | $7.99  | ADD TO CART  |
        | Test.allTheThings() T-Shirt (Red) | This classic Sauce Labs t-shirt is perfect to wear when cozying up to your keyboard to automate a few tests. Super-soft and comfy ringspun combed cotton.              | $15.99 | ADD TO CART  |
    When User change sorting to Name (Z to A) on inventory page
    Then Products names should be in order on inventory page
      | Test.allTheThings() T-Shirt (Red) |
      | Sauce Labs Onesie                 |
      | Sauce Labs Fleece Jacket          |
      | Sauce Labs Bolt T-Shirt           |
      | Sauce Labs Bike Light             |
      | Sauce Labs Backpack               |
    When User change sorting to Price (low to high) on inventory page
    Then Products names should be in order on inventory page
      | Sauce Labs Onesie                 |
      | Sauce Labs Bike Light             |
      | Sauce Labs Bolt T-Shirt           |
      | Test.allTheThings() T-Shirt (Red) |
      | Sauce Labs Backpack               |
      | Sauce Labs Fleece Jacket          |
    When User change sorting to Price (high to low) on inventory page
    Then Products names should be in order on inventory page
      | Sauce Labs Fleece Jacket          |
      | Sauce Labs Backpack               |
      | Sauce Labs Bolt T-Shirt           |
      | Test.allTheThings() T-Shirt (Red) |
      | Sauce Labs Bike Light             |
      | Sauce Labs Onesie                 |
    When User adds to cart Sauce Labs Backpack product on inventory page
    Then Product Sauce Labs Backpack should have REMOVE visible button name
      And Cart should have 1 displayed on page header
    When User goes to shopping cart
    Then Shopping cart page should be displayed
      And Cart should have selected products
        | name                | description                                                                                                                            | price  | buttonStatus |
        | Sauce Labs Backpack | carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection. | $29.99 | REMOVE       |
    When User clicks checkout button on shopping cart page
    Then Checkout step one page should be displayed
    When User fills out client information on checkout step one page
      | firstName        | lastName     | zipCode |
      | StandardUserName | UserLastName | 54-999  |
    Then Checkout step two page should be displayed
      And Overview information should have product information checkout step two page
        | name                | description                                                                                                                            | price  |
        | Sauce Labs Backpack | carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection. | $29.99 |
      And Overview information should have summary information on checkout step two page
        | paymentInformation | shippingInformation         | itemTotal | tax   | total  |
        | SauceCard #31337   | FREE PONY EXPRESS DELIVERY! | $29.99    | $2.40 | $32.39 |
    When Users clicks finish button to finalize transaction on checkout step two page
    Then Checkout complete page should be displayed
      And Order confirmation messages should be displayed
        | THANK YOU FOR YOUR ORDER |
        | Your order has been dispatched, and will arrive just as fast as the pony can get there! |