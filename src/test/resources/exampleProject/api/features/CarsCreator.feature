Feature: Create new cars

  Background: Common setup for scenarios
    Given Setup base request specification without auth
    When User sends a valid POST request to login
    Then Response status should be 200

  @dev
  Scenario: CC01 Existing BMW 750i engine from cars should be as expected
    When User sends a valid GET request to cars to get all
    Then Response status should be 200
      And Cars response should have car details
        | brand | model | generation | displacement | cylinderSystem | power |
        | BMW   | 750i  | 6          | 4395         | V8             | 450   |

  Scenario: CC02 Newly created car by cars endpoint should have saved all details
    When User updates cars request with data
      | brand | model | generation | displacement | cylinderSystem | power |
      | BWM   | 735   | 4          | 4395         | V8             | 305   |
      And User sends a valid POST request to cars
    Then Response status should be 200
    When User sends a valid GET request to cars to get newly created car
    Then Response status should be 200
      And Cars response should have car details
        | brand | model | generation | displacement | cylinderSystem | power |
        | BWM   | 735   | 4          | 4395         | V8             | 305   |
    When User updates cars request with data
      | brand | model | generation | displacement | cylinderSystem | power |
      | BWM   | 735   | 4          | 4395         | V8             | 305   |
      And User sends a valid PUT request to update car with 1 id
