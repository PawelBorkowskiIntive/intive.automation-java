Feature: Check if app is up and running
  
  Background: Common setup for scenarios
    Given Setup base request specification
      And Set user credentials for Admin

  Scenario: BO01 Users creates booking and is able to assert created booking
    When Booking request is updated with
      | firstname | lastname       | totalprice |
      | Arnold    | Schwarzenegger | 999        |
      And User sends a valid POST request to Booking
    Then Response status should be 200
      And Created booking should have data
        | firstname | lastname       | totalprice |
        | Arnold    | Schwarzenegger | 999        |
    When User sends a valid GET request to Booking looking for created booking by Id
    Then Booking response should have data
      | firstname | lastname       | totalprice | depositpaid | checkin    | checkout   | additionalneeds |
      | Arnold    | Schwarzenegger | 999        | true        | 2018-01-01 | 2019-01-01 | Breakfast       |
