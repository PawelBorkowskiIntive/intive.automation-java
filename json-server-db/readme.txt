Open VS Code and run JSON-SERVER with command:
json-server db.json -m .\node_modules\json-server-auth\

API URI: /cars

Whole API URI: http://localhost:3000/660/cars
Login URI: http://localhost:3000/login (POST)

User credentials:
{
  "email": "testUser@mail.com",
  "password": "Password"
}
